import json
from dl_lr import DL_LR

with open('temp.json', 'r') as file:

    # Load the JSON data into a Python dictionary
    block_json = json.load(file)

dllr = DL_LR()
key_words = dllr.get_keywords_block_wise(block_json)
json_formatted_str = json.dumps(block_json, indent=2)

f = open("temp.json", "w")
f.write(json_formatted_str)
f.close()
