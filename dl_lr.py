import utils
import nltk
from nltk.corpus import stopwords
stop_words = set(stopwords.words('english'))
import tensorflow as tf
import pandas as pd
import phrase_extractor
import pickle
import json


class DL_LR:

	def __init__(self):
		self.dataset = ""
		pass
		
	def generate_dataset(self, doc, keyphr = None):
		self.universal_tagset = {'ADJ':1, 'ADP':2, 'ADV':3,'NOUN':4,  'DET':6,'VERB':5 , 'NUM':7, 'PRON':8, 'PRT':9, 'CONJ':10, 'X':11, '.':12}

		self.lines = [[doc] , [keyphr]]
		tokens = nltk.word_tokenize(self.lines[0][0])
		self.tokens = tokens
		dataset = []
			
		for i in range(0, len(tokens)):
			dataset_ele = []
			dataset_ele.append(tokens[i])
			dataset_ele.append(self.universal_tagset[nltk.pos_tag([tokens[i]], tagset = "universal")[0][1]])
			dataset_ele.append(len(tokens[i]))
		    
			if tokens[i] in stop_words:
				dataset_ele.append(0)
			else:
				dataset_ele.append(1)
			   
			if(i > 0 and dataset[-1][1] < 5 and dataset_ele[1] < 5  and i%5):
				dataset_ele.append(1)
			else:
				dataset_ele.append(0)
		#	dataset_ele.append(1)

			dataset_ele.append("na")

			if(ord(tokens[i][0]) >= 65 or ord(tokens[i][0]) <= 90):
				dataset_ele.append(1)
			else:
				dataset_ele.append(0) 
			dataset.append(dataset_ele) 
		new_model = tf.keras.models.load_model('lstm_model.h5')
		i = 0
		j = 0
		X, Y = utils.get_variables(self.lines[0] , self.lines[1])
		yhats = new_model.predict(X)
		flag = 0
		for yhat in yhats:
			y_cat = utils.dl_get_values(yhat)
			for unit in y_cat:
				try:
					dataset[j][-2] = unit
				except:
					break
				j = j + 1
				if(flag == 1):
					break
   
		self.dataset = dataset
		return dataset
		
	def extract_keyphrases(self):
		self.colms = ['pos tag',	'size',	'is stop word',	'is prev keyword',	'DL pred output',	'is start capital']
		prev = 0
		keywords = []
		pickled_model = pickle.load(open('ml_model.pkl', 'rb'))
		dataset = self.dataset 
		index = 0
		for dataset_ele in dataset:
		#	print(dataset_ele)
			instance = {}
			i = 1
			for col in self.colms:
				instance[col] = [dataset_ele[i]]
				i = i + 1
		#	instance['is prev keyword'] = [prev]
		 #   print(instance)
			instance = pd.DataFrame.from_dict(instance)
		 #   print(instance)
			y_pred = pickled_model.predict(instance)
			if(y_pred[0] == 1):
		 #       print(dataset_ele[0], " ", end="")
				keywords.append([dataset_ele[0], index])
				prev = 1
			else:
				prev = 0
			index = index + 1

		
		
		keyphrases = self.execute_epochs(keywords,pickled_model, 5)
		
		
#		print(keywords)	
#		keyphrases = set(phrases)					
		diff = set()
#		for ele1 in keyphrases:
#			for ele2 in keyphrases:
#				if(ele1 == ele2):
#					continue
#				if(ele1.find(ele2) != -1):
#					diff.add(ele2)
		
#		keyphrases = keyphrases -diff
		present = set()
		op = []
		for keyphrase in keyphrases:
			keyph = ""
			for word in keyphrase:
				keyph = keyph + " "  +  word
			if keyph in present:
				continue
			else:
				op.append([keyphrase, 1])
				present.add(keyph)
		

		return op
	
	def get_keywords_block_wise(self,block_json, no_of_kw = 7):
		
		for key, items in block_json.items():
			text = items["text"]
			self.generate_dataset(text)
			keywords = self.extract_keyphrases()
			for i in range(0, len(keywords)):
				keywords[i] = list(keywords[i])
#				keywords[i][0] = nltk.word_tokenize(keywords[i][0])
			block_json[key]["dl_lr-keywords"] = keywords
			
		return block_json
		
	def execute_epochs(self, keywords, ml_model, epochs):
		dataset = self.dataset
		
		for epoch in range(epochs):
			for keyword in keywords:
				dataset[keyword[1]][4] = 1
				
			keywords2 = []
			index = 0
			for dataset_ele in dataset:
			#	print(dataset_ele)
				instance = {}
				i = 1
				for col in self.colms:
					instance[col] = [dataset_ele[i]]
					i = i + 1
			#	instance['is prev keyword'] = [prev]
			 #   print(instance)
				instance = pd.DataFrame.from_dict(instance)
			 #   print(instance)
				y_pred = ml_model.predict(instance)
				if(y_pred[0] == 1):
			 #       print(dataset_ele[0], " ", end="")
					keywords2.append([dataset_ele[0], index])
					prev = 1
				else:
					prev = 0
				index = index + 1
			keywords = keywords + keywords2
			
		nos = set()
		for kw in keywords:
			nos.add(kw[1])
		nos = sorted(list(nos))
		keyphrases = list(self.generate_keyphrases(self.tokens, nos))	
		return keyphrases
		
	def generate_keyphrases(self, tokens, nos):
		self.super_keyphrases = []
		self.recurssion(tokens, list(nos), 0, [])
		max_score = 0
		K = []
		for k in self.super_keyphrases:
			score = self.get_scores(k)
			if(score > max_score):
				max_score = score
				K = k
		
		n = len(K)-1
		self.super_keyphrases = []
		while(n > 0):
			end = K[n]+1
			beg = K[n-1]
			self.super_keyphrases.append(tokens[beg:end])
			n = n - 2	
		
		nos = set(nos).difference(set(K))
		keywords = []
		if(len(self.super_keyphrases) < 3):
			keywords = set([self.tokens[i] for i in nos])
			keywords = self.filter_keywords(keywords, [1,3,4])
		
		return self.super_keyphrases + keywords
		
	def recurssion(self, tokens, nos, n, keyphrases):
	
		if(n >= len(nos)-1):
			self.super_keyphrases.append(keyphrases)
			return 
	
		for i in range(n+1, len(nos)):
			if(nos[i] - nos[n] > 3):
				self.recurssion(tokens, nos, n+1, keyphrases)
				break
			
			elif(nos[i] - nos[n] == 3):
				tag1 = self.universal_tagset[nltk.pos_tag([tokens[nos[n]+1]], tagset = "universal")[0][1]]
				tag2 = self.universal_tagset[nltk.pos_tag([tokens[nos[n]+2]], tagset = "universal")[0][1]]
				if(tag1 > 5 and tag2 > 5 and tag1 > 5 and tag2 > 5):
					self.recurssion(tokens, nos, n+1, keyphrases)
					continue
				elif(tag1 > 10 or tag2 > 10):
					self.recurssion(tokens, nos, n+1, keyphrases)
					continue
				else:
					self.recurssion(tokens, nos, n+1, keyphrases)
					self.recurssion(tokens, nos, n+2, keyphrases + [nos[n], nos[i]])
					
			elif(nos[i] - nos[n] == 2):
				tag1 = self.universal_tagset[nltk.pos_tag([tokens[nos[n]+1]], tagset = "universal")[0][1]]
				if(tag1 > 10 and tag2 > 10):
					self.recurssion(tokens, nos, n+1, keyphrases)
					continue
				else:
					self.recurssion(tokens, nos, n+1, keyphrases)
					self.recurssion(tokens, nos, n+2, keyphrases + [nos[n], nos[i]])
				
			else:
				self.recurssion(tokens, nos, n+1, keyphrases)
				self.recurssion(tokens, nos, n+2, keyphrases + [nos[n], nos[i]])	
	
	
	def get_scores(self, arr):
		
		if(len(arr) == 0 ):
			return 0
		
		keyphrase_number = len(arr)/2
		
		distance = 0
		i = len(arr)-2
		while(i >= 0 and i - 1 >= 0):
			distance = distance + abs(arr[i] - arr[i-1])
			i = i - 2
		avg_dist = distance / keyphrase_number
		
		score = ((avg_dist)**(1/2))*(keyphrase_number)**(3) 
		return score	
		
	def filter_keywords(self, keywords, accepted_tags):
		op = []
		for keyword in keywords:
			tag = self.universal_tagset[nltk.pos_tag([keyword], tagset = "universal")[0][1]]	
			if tag in accepted_tags:
				if(len(keyword) > 5):
					op.append([keyword])
		return op
		
dfsvm = DL_LR()
#data = pd.read_csv('galvin_dataset.csv')
#doc = data["Blog"][280]
#keyphr = data["Keyphrases"][100]
#dfsvm.generate_dataset(doc, None)
#print(dfsvm.extract_keyphrases())



#f = open("../Resource/S0010938X15002085.txt")
#doc = f.read()

#block_json = utils.get_sentence_blocks(doc)
#key_words = dfsvm.get_keywords_block_wise(block_json)
#json_formatted_str = json.dumps(block_json, indent=2)
#torch.cuda.empty_cache()

#f = open("temp.json", "w")
#f.write(json_formatted_str)
#f.close()
