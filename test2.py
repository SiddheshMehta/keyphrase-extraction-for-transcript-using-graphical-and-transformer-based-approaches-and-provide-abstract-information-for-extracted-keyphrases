from keybert import KeyBERT
import re

# Load pre-trained BERT model
model = KeyBERT()

# Define document
document = "The quick brown fox jumps over the lazy dog."

# Remove punctuations and convert to lower case
document = re.sub(r'[^\w\s]', '', document.lower())

# Extract continuous keyphrases
keyphrases = model.extract_keywords(document, keyphrase_ngram_range=(1, 4))

# Print the extracted keyphrases
print(keyphrases)
