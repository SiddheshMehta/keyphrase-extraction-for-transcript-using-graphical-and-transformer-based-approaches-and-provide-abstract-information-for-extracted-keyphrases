import json
import os
import nltk
import csv
import nltk
import torch
from transformers import BertTokenizer, BertModel
import pandas as pd
import numpy as np
import tensorflow as tf
import random
from tensorflow.keras.layers import Bidirectional, Dropout, TimeDistributed
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense



tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
model = BertModel.from_pretrained('bert-base-uncased')

#assign directory
directory = "dataset"

#iterate over files in that directory
all_data = ""
for filename in os.listdir(directory):
    f = os.path.join(directory, filename)
    #checking if it is a file
    count = 0
    if os.path.isfile(f):
        json_file = open(f, "r")
        data = json_file.read()
        json_file.close()
        all_data = all_data + data
        

json_list = all_data.split("\n")



    
    
def get_doc_from_jsonl(jsonl_list):
    x = 0
    doc_list = []
    keyphrase_list = []
    for jsonl_ele in jsonl_list:
        if(x%100 == 0):
            print(x)
        x = x + 1
        if(len(jsonl_ele) < 10):
            continue
        json_ele = json.loads(jsonl_ele)
        documents = json_ele["document"]
        if(len(documents) > 450 or len(documents) < 20):
            continue
        keyphrases = json_ele["abstractive_keyphrases"]
        
        tokenized_keyphrases = []
        for i in range(0, len(keyphrases)):
            keyphrases[i] = nltk.word_tokenize(keyphrases[i])
            tokenized_keyphrases.append(keyphrases[i])
        keyphrase_list.append(tokenized_keyphrases)
    
        doc = []
        for  i in range(0, len(documents)):
            doc = doc + nltk.word_tokenize(documents[i])
        doc_list.append(doc)
    
    return doc_list, keyphrase_list
    
doc_list, keyphrase_list = get_doc_from_jsonl(json_list)
 
# opening the CSV file

if(True):
   
  # reading the CSV file
  
  df = pd.read_csv(r'galvin_dataset.csv', encoding = "ISO-8859-1")
  df["Blog"].str.encode('ascii', 'ignore').str.decode('ascii')
  df["Keyphrases"].str.encode('ascii', 'ignore').str.decode('ascii')
  
  for M in range(0, len(df["Blog"][:50])):
 
  # displaying the contents of the CSV file
  #for lines in csvFile:
        lines = [df["Blog"][M] , df["Keyphrases"][M]]
        keyphrases = [keyphr.strip('\'\'')  for keyphr in  lines[1].strip('][').split(', ')]
        text = nltk.word_tokenize(lines[0])
        doc = []
        i = 0
        while(i < len(text)):
            if(text[i][-1] != '-'):
                doc.append(text[i])
            elif(i != len(text)-1):
                doc.append(text[i][:-1]+text[i+1])
                i = i + 1
            i = i + 1
        doc_list.append(doc)
        
        k_phrases = []
        for keyphrase in keyphrases:
            keyphrase = nltk.word_tokenize(keyphrase)
            keyphr = []
            i = 0
            while(i < len(keyphrase)):
                if(keyphrase[i][-1] != '-'):
                    keyphr.append(keyphrase[i])
                elif(i != len(keyphrase)-1):
                    keyphr.append(keyphrase[i][:-1]+keyphrase[i+1])
                    i = i + 1
                i = i + 1
            k_phrases.append(keyphr)
        keyphrase_list.append(k_phrases)
            


def get_bio_tags(doc, keyphrases):
    bio_tags = ["O" for i in range(0, len(doc))]
    count = 0
    j = 0
    for i in range(0, len(keyphrases)):
        keyphrase = keyphrases[i]
        if(len(keyphrase) == 0):
            continue
        for j in range(0, len(doc)):
            if(doc[j].lower() == keyphrase[0].lower()):
                J = j
                flag = 0
                for k in range(0, len(keyphrase)):
                    if(J < len(doc)):
                        if(keyphrase[k].lower() != doc[J].lower()):
                            flag = 1
                            break
                        else:
                            pass
                    else:
                        continue
                    J = J + 1
                if(flag == 0):
                    bio_tags[j] = 'B'
                    J = j+1
                    for x in range(1, len(keyphrase)):
                        if(J >= len(bio_tags)):
                            break
                        bio_tags[J] = "I"
                        J = J + 1
                    count = count + 1
                if(flag == 1):
                    pass
        
    return bio_tags

bio_tags_list = []
n = len(doc_list) 
for i in range(0, n):
    bio_tags = get_bio_tags(doc_list[i], keyphrase_list[i])
    print(bio_tags)
    bio_tags_list.append(bio_tags)

for i in range(0, n):
	print(len(doc_list[i]), len(bio_tags_list[i]))
	if(len(doc_list[i]) != len(bio_tags_list[i])):
		print("Error")    
    

embedding_doc = []
j = 0
noise = []
for doc in doc_list:
    doc_text = ""
    for ele in doc:
        doc_text = doc_text + " " + ele
    tokens = nltk.word_tokenize(doc_text)
    input_ids = torch.tensor([tokenizer.convert_tokens_to_ids(tokens)])
    print(j, len(keyphrase_list[j]))
    j = j + 1
    try:
        with torch.no_grad():
            outputs = model(input_ids)
            embeddings = outputs[0]
            if(len(embeddings[0]) != len(tokens) or len(embeddings[0]) != len(bio_tags_list[j-1])):
                bio_tags_list[j-1] = get_bio_tags(tokens, keyphrase_list[j-1])
            if(len(embeddings[0]) != len(tokens) or len(embeddings[0]) != len(bio_tags_list[j-1])):
                print("YES I caught u error")      
    except:
        noise.append(j-1)
        continue
            
    # Extract embeddings for each token in the input text
    word_embeddings = []
    for i in range(len(tokens)):
        word_embedding = embeddings[0][i]
        word_embeddings.append(word_embedding)

    embedding_doc.append(word_embeddings)
    
for i in range(0, len(noise)):
    bio_tags_list.pop(noise[i]-i)
    
print(len(bio_tags_list), len(doc_list), len(embedding_doc))  

for i in range(0, n-10):
	print(len(embedding_doc[i]), len(bio_tags_list[i]))
	if(len(embedding_doc[i]) != len(bio_tags_list[i])):
		print("Error")
  
time_steps = 3
#data = np.array(embedding_doc)
#for i in range(0, len(data)):
#    data[i] = np.array(data[i])
#    for j in range(0, len(data[i])):
#        data[i][j] = np.asarray(data[i][j]).astype(np.float32)
#X = data

# Get the maximum sequence length and embedding size
max_sequence_length = max(len(seq) for seq in embedding_doc)
embedding_size = len(embedding_doc[0][0])

# Initialize the data array with zeros
num_samples = len(embedding_doc)
X = np.zeros((num_samples, 500, embedding_size), dtype=np.float32)

# Copy the embeddings into the data array
for i, seq in enumerate(embedding_doc):
    for j, emb in enumerate(seq):
        X[i, j, :] = np.asarray(emb, dtype=np.float32)
        
        
op = []
for bio_tags in bio_tags_list:
    ele = []
    for tag in bio_tags:
        if(tag == "O"):
            ele.append([1,0,0])
        elif(tag == "B"):
            ele.append([0,1,0])
        elif(tag == "I"):
            ele.append([0,0,1])
        
    op.append(ele)
#op = np.array(op)



max_sequence_length = max(len(seq) for seq in op)
op_size = len(op[0][0])

# Initialize the data array with zeros
num_samples = len(op)
Y = np.zeros((num_samples, 500, op_size), dtype=np.float32)

# Copy the embeddings into the data array
for i, seq in enumerate(op):
    for j, emb in enumerate(seq):
        Y[i, j, :] = np.asarray(emb, dtype=np.float32)
        
print(X.shape, Y.shape) 

for i in range(0, n-10):
	if(len(X[i]) != len(Y[i])):
		print("Error", len(X[i]), len(Y[i]))      
     
nodes = 768
out_size = 3 
if(True):
	if(True):
		model = Sequential()
		model.add(LSTM(units=nodes, input_shape=(X.shape[1], X.shape[2]),activation = "tanh", recurrent_activation = "hard_sigmoid", return_sequences=True))
		model.add(Dropout(0.25))
		model.add(TimeDistributed(Dense(384, activation="relu"))) 
		model.add(Dropout(0.25))
		model.add(TimeDistributed(Dense(100, activation="relu"))) 
		model.add(Dropout(0.25))
		model.add(TimeDistributed(Dense(out_size ,activation="softmax")))  # For the output a Dense (fully connected) layer is used.
		model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
		model.fit(X[:100], Y[:100],validation_data=(X[150:160], Y[150:160]), batch_size=10, epochs=5)

print(X[750:].shape)
loss_and_metrics = model.evaluate(X[160:], Y[160:], verbose=0)
metrics_names = model.metrics_names
print(metrics_names)
accuracy = loss_and_metrics[metrics_names.index('accuracy')]
print('Test accuracy:', accuracy)

from keras.models import load_model

model.save("lstm_model.h5")
