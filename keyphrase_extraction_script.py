import json
import keyphrase_extraction as ke

block_json = ke.extract_keyphrases(5)


json_formatted_str = json.dumps(block_json, indent=2)
f = open("doc_keyphrases.json", "w")
f.write(json_formatted_str)
f.close()
