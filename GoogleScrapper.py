from bs4 import BeautifulSoup
import requests
import time
from googlesearch import search

def google_search(keyword):
    
    query = keyword
    results = []
    for result in search(query, tld="co.in", num=5, stop=5, pause=2): 
        results.append(result) 
    return results

def data_scrap(uri, path):
	single_page = []
	i = 0
	while(i < 5):
		try:
			r = requests.get(uri)
		except:
			continue
		i = i + 1
		htmldata = r.text
		   #print(len(r.text))
		soup = BeautifulSoup(htmldata, 'html.parser')
		data=''
		for data in soup.find_all("p"):
			single_page.append(data.get_text())
		text = ""
		for data in single_page:
			text = text + " "+ data
			
		file_path = path + "def" + str(i) + ".txt"
		f = open(file_path, "w")
		f.write(text[0:1536])
		f.close()		
		

#results = google_search("adam seeker")
#data = data_scrap(results)
#print(data)
