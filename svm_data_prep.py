import pandas as pd
import utils
import nltk
from nltk.corpus import stopwords
import csv
import tensorflow as tf

new_model = tf.keras.models.load_model('lstm_model.h5')


universal_tagset = {'ADJ':1, 'ADP':2, 'ADV':3, 'CONJ':4, 'DET':5, 'NOUN':6, 'NUM':7, 'PRON':8, 'PRT':9, 'VERB':10, 'X':11, '.':12}

colns = ["word", "pos tag", "size", "is stop word", "is prev keyword", "DL pred output", "is start capital", "y_pred"]

df = pd.read_csv(r'galvin_dataset.csv', encoding = "ISO-8859-1")
df["Blog"].str.encode('ascii', 'ignore').str.decode('ascii')
df["Keyphrases"].str.encode('ascii', 'ignore').str.decode('ascii')

dataset = []




stop_words = set(stopwords.words('english'))

for M in range(0, len(df["Blog"])):
 
  # displaying the contents of the CSV file
  #for lines in csvFile:
	lines = [df["Blog"][M] , df["Keyphrases"][M]]
	keyphrases = [keyphr.strip('\'\'')  for keyphr in  lines[1].strip('][').split(', ')]
	df["Keyphrases"][M] = [nltk.word_tokenize(keyphrase) for keyphrase in keyphrases]


for M in range(0, len(df["Blog"])):	
	lines = [df["Blog"][M] , df["Keyphrases"][M]]
	tokens = nltk.word_tokenize(lines[0])
	bio_tags = utils.get_bio_tags(tokens, df["Keyphrases"][M])
	
	
	for i in range(0, len(tokens)):
		dataset_ele = []
		dataset_ele.append(tokens[i])
		dataset_ele.append(universal_tagset[nltk.pos_tag([tokens[i]], tagset = "universal")[0][1]])
		dataset_ele.append(len(tokens[i]))
		
		if tokens[i] in stop_words:
			dataset_ele.append(0)
		else:
			dataset_ele.append(1)
		
		if(i > 0 and (bio_tags[i-1] == "B" or  bio_tags[i-1] == "I")):
			dataset_ele.append(1)
		else:
			dataset_ele.append(0)
		
		dataset_ele.append("na")
		
		if(ord(tokens[i][0]) >= 65 or ord(tokens[i][0]) <= 90):
			dataset_ele.append(1)
		else:
			dataset_ele.append(0) 
		
		if(bio_tags[i] == "B" or  bio_tags[i] == "I"):
			dataset_ele.append(1)
		else:
			dataset_ele.append(0)
			
		dataset.append(dataset_ele)

i = 0
j = 0
while(i+100 <= len(df["Blog"])):	
	try:
		X, Y = utils.get_variables(df["Blog"][i:i+100].tolist() , df["Keyphrases"][i:i+100].tolist())
		yhats = new_model.predict(X)
		flag = 0
		for yhat in yhats:
			y_cat = utils.dl_get_values(yhat)
			for unit in y_cat:
				try:
					dataset[j][-3] = unit
				except:
					break
				j = j + 1
			if(flag == 1):
				break	
		i = i + 100
	except:
		with open(filename, 'w') as csvfile: 
		    # creating a csv writer object 
			csvwriter = csv.writer(csvfile) 
			   
		    # writing the fields 
			csvwriter.writerow(colns) 

filename = "ML_categorize_dataset.csv"
    
with open(filename, 'w') as csvfile: 
    # creating a csv writer object 
	csvwriter = csv.writer(csvfile) 
        
    # writing the fields 
	csvwriter.writerow(colns) 
        
    # writing the data rows 
	csvwriter.writerows(dataset)	
