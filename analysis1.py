import matplotlib.pyplot as plt
import numpy as np

avg = (0.5693 + 0.5324 + 0.5546 + 0.5274 + 0.5622)/5

x = np.array(['T1','T2','T3','T4','T5', 'Average'])
y = np.array([ 0.5693 , 0.5324 , 0.5546 , 0.5274 , 0.5622, avg])

plt.plot(x, y,  marker='s', color='maroon')
plt.xlabel("Transcripts")  # add X-axis label
plt.ylabel("F measure")  # add Y-axis label
plt.title("F score")  # add title
plt.show()



