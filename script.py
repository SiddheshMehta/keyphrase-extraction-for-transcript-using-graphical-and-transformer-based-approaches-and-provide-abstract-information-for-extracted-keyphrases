from multiprocessing import Process
import subprocess

class py_script_executor:
	def execute_serial(self,script_name , args = []):
		for arg in args:
			script_name = script_name + " " + arg
		try:
			subprocess.check_output("python3 " + script_name , shell=True)
		except:
			pass
	
	def execute_parallel(self, script_name, args = []):
		try:
			p = Process(target=self.execute_serial, args=(script_name, args))
			p.start()
		except:
			pass

		
#scriptExecutor = py_script_executor()
#scriptExecutor.execute_serial("preprocess_script.py")
