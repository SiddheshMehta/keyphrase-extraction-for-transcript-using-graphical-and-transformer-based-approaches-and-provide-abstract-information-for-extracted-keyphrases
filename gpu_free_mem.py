import subprocess
import nltk

# Execute the command and capture the output
output = subprocess.check_output('nvidia-smi >> gpu_pid' , shell=True)

# Print the output
f = open("gpu_pid", "r")
op_text = f.read()
f.close()
f = open("gpu_pid", "w")
f.close()

lines = op_text.split("\n")
i = len(lines) - 3
print(i)
while(i > 0):
	tokens = nltk.word_tokenize(lines[i])
	if(tokens[5] != "C"):
		break
	output = subprocess.check_output('kill ' + tokens[4] , shell=True)
	i = i - 1
