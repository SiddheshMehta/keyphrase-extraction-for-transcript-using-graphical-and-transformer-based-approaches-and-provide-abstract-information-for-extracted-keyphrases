import nltk

universal_tagset = {'ADJ':1, 'ADP':2, 'ADV':3, 'CONJ':4, 'DET':5, 'NOUN':6, 'NUM':7, 'PRON':8, 'PRT':9, 'VERB':10, 'X':11, '.':12}

standard_phrase_tags = [	['ADJ', 'NOUN'],
					['NOUN', 'NOUN'],
					['ADJ', 'ADJ', 'NOUN'],
					['ADJ', 'NOUN', 'NOUN'],
					['NOUN', 'ADJ', 'NOUN'],
					['NOUN', 'NOUN', 'NOUN'],
					['NOUN', 'ADP', 'NOUN'],
					['NOUN', 'NOUN', 'NOUN', 'NOUN']	]

def get_hash_code_for_substring(seq):
	hash_value = 0
	for i in range(0, len(seq)):
		hash_value = hash_value + universal_tagset[seq[i]] * (len(universal_tagset)**i)
	return hash_value	
		
def extract_phrases(doc):

	phrase_address = []

	sentences = nltk.sent_tokenize(doc)
	pos_tagged_sentences = []
	for sentence in sentences:
		pos_tagged_sentences.append(nltk.pos_tag(nltk.word_tokenize(sentence), tagset = "universal"))
	
	hash_codes = set()
	phrase_sizes = set()
	for phrase_tag in standard_phrase_tags:
		phrase_sizes.add(len(phrase_tag))
		seq = phrase_tag[::-1]
		hash_value = get_hash_code_for_substring(seq)
		hash_codes.add(hash_value) 
	
	sentence_number = -1
	for tagged_sentence in pos_tagged_sentences:
		sentence_number += 1
		for m in phrase_sizes:
			n = len(tagged_sentence)
			x = 0
			hash_code = 0
			phrase_tag = []
			for i in range(0, n):
				if(i - x + 1 < m):
					phrase_tag.append(tagged_sentence[i][1])
					continue
				else:
					if(i - x + 1 == m and x == 0):
						phrase_tag.append(tagged_sentence[i][1])
						hash_code = get_hash_code_for_substring(phrase_tag[::-1])
#						print(x,hash_code- universal_tagset[tagged_sentence[x][1]]*(len(universal_tagset)**(m-1)))
					else:
						hash_code = (hash_code - universal_tagset[tagged_sentence[x-1][1]]*(len(universal_tagset)**(m-1)))*len(universal_tagset) + universal_tagset[tagged_sentence[i][1]]
						if hash_code in hash_codes:
							phrase_address.append([sentence_number, x, i])
					x = x + 1
					
	phrases = []
	for address in phrase_address:
		sent_no, start, end = address
		phrase = []
		for i in range(start, end + 1):
			phrase.append(pos_tagged_sentences[sent_no][i][0])
		phrases.append(phrase)
	return phrases
					
		
#f = open("../Resource/transcript1.txt", "r")
#doc = f.read()
#print(extract_phrases(doc))	
