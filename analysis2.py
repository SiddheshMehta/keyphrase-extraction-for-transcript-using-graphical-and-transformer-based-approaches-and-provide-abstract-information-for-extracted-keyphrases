import numpy as np 
import matplotlib.pyplot as plt 
  
X = ['T1','T2','T3','T4', 'T5']
pr = [0.5693, 0.5324, 0.5546, 0.5274, 0.5622]
rc = [0.5121, 0.5315, 0.5613, 0.5311, 0.5512]
  
X_axis = np.arange(len(X))
  
plt.bar(X_axis - 0.2, pr, 0.4, label = 'Precission')
plt.bar(X_axis + 0.2, rc, 0.4, label = 'Recall')
  
plt.xticks(X_axis, X)
plt.xlabel("Transcripts")
plt.ylabel("Precission and Recall")
plt.title("Number of Students in each group")
plt.legend()
plt.show()




