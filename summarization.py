from googlesearch import search
import requests
from bs4 import BeautifulSoup
import wikipediaapi
import torch
import json 
from transformers import T5Tokenizer, T5ForConditionalGeneration, T5Config

model = T5ForConditionalGeneration.from_pretrained('t5-small')
tokenizer = T5Tokenizer.from_pretrained('t5-small')
device = torch.device('cpu')

def google_scrap(query):
    # List to store the extracted text from the web pages
    text_list = []

    for url in search(query, stop=10):
        try:
            # Send a request to the URL and get the HTML content
            response = requests.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')

            # Extract all the text from the <p> tags
            paragraphs = soup.find_all('p')
            text = ""
            for p in paragraphs:
                text += p.get_text() + " "

            text_list.append(text)
        except requests.exceptions.RequestException as e:
            print(f"Error requesting {url}: {e}")
        except Exception as e:
            print(f"Error scraping {url}: {e}")
            pass

    combined_text = ""
    for i in text_list:
        combined_text = combined_text + i

    return combined_text


def chunk_data(data):
	chunk_list = []

	# Calculate the total number of chunks based on the length of the data and the chunk size
	num_chunks = len(data) // 512 + (len(data) % 512 > 0)

	# Loop through the data and break it into chunks of 512 bytes
	for i in range(num_chunks):
	    # Calculate the start and end indices of the current chunk
	    start_index = i * 512
	    end_index = start_index + 512

	    # Extract the current chunk from the data
	    chunk = data[start_index:end_index]

	    # Add the chunk to the chunk list
	    chunk_list.append(chunk)
	return chunk_list



def wikipedia_information(query):
	# Create a Wikipedia API object
	wiki = wikipediaapi.Wikipedia('en')

	# Define the keyword you want to search for
	keyword = query

	# Get the Wikipedia page for the keyword
	page = wiki.page(keyword)

	# Store the page content in a string
	page_content = page.text
	
	return page_content

def summarize_keyword(data):

	all_summaries = []
	track = 0;
	for i in data:
		if(track >= 3):
			break

		preprocess_text = i.strip().replace("\n","")
		t5_prepared_Text = "summarize: "+preprocess_text
		#print ("original text preprocessed: \n", preprocess_text)

		tokenized_text = tokenizer.encode(t5_prepared_Text, return_tensors="pt").to(device)


		# summmarize 
		summary_ids = model.generate(tokenized_text,
				                    num_beams=4,
				                    no_repeat_ngram_size=2,
				                    min_length=0,
				                    max_length=100000,
				                    early_stopping=True)

		output = tokenizer.decode(summary_ids[0], skip_special_tokens=True)

		all_summaries.append(output)
		track = track + 1
	return all_summaries

query = input("Enter keyword:\n")
data = wikipedia_information(query)
if len(data) == 0:
	data = google_scrap(query)
chunks = chunk_data(data)
all_summaries = summarize_keyword(chunks)
print(all_summaries)
