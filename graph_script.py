
import json
import Graph
import Pooling as pl
import sys

f = open(sys.argv[1], "r")
doc = f.read()
f.close()
print("Doc :", doc)

with open('temp.json', 'r') as file:

    # Load the JSON data into a Python dictionary
    block_json = json.load(file)


graph = Graph.Graph()
graph.addText(doc)
graph.build()
scores = graph.get_statistics()
graph.rank_phrases_json(block_json)

json_formatted_str = json.dumps(block_json, indent=2)
f = open("temp.json", "w")
f.write(json_formatted_str)
f.close()



