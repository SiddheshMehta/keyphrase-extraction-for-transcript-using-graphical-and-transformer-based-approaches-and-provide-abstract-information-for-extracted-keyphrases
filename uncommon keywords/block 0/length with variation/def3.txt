 An official website of the United States government 
The .gov means it’s official.

            Federal government websites often end in .gov or .mil. Before
            sharing sensitive information, make sure you’re on a federal
            government site.
           
The site is secure.

            The https:// ensures that you are connecting to the
            official website and that any information you provide is encrypted
            and transmitted securely.
           

          Objective:
        
      
      To investigate the current epidemiology of menstrual patterns among women of fertile age.
     

          Design:
        
      
      Cross-sectional postal questionnaire study.
     

          Setting:
        
      
      County of Copenhagen, Denmark.
     

          Subjects:
        
      
      3743 women, aged 15-44, selected at random from a Danish county, who were asked to provide information on menstrual pattern during the preceding year, 1981. The response rate was 78%. Information from non-responders was obtained via telephone interviews.
     

          Results:
        
      
      In women with regular menstrual cycles, the 5th-95th centile range of usual cycle length decreased from 23-35 days in the 15-19 years age group to 23-30 days in the 40-44 years age group. Only 0.5% of regular menstruating women had a usual cycle length of less than 21 days and 0.9% had a usual cycle length of greater than 35 days. At least one cycle length of less than 21 days was experienced by 18.6%, whereas 29.5% had at least one cycle of greater than 35 days. Menstrual cycle variation of greater than 14 days was present in 29.3% of all women. Cycle length variation of greater than 14 days was 2.7 times more frequent in women from lower social groups (controlled by age).
     

          Conclusion:
        
      
      The study confirmed the normally used definitions of polymenorrhoea (cycle length less than 21 days) and oligomenorrhoea (cycle length between 36 and 90 days), as these very short or long menstrual cycle lengths were very seldom recorded for a longer period. However, the high frequency in a normal population of large menstrual cycle length variation challenges the view that an intra-individual variation of greater than 5 days should be regarded as a sign of disease in the woman.
     NCBI Literature Resources 
MeSH
PMC
Bookshelf
Disclaimer
 The PubMed wordmark and PubMed logo are registered trademarks of the U.S. Department of Health and Human Services (HHS). Unauthorized use of these marks is strictly prohibited. Connect with NLM National Library of Medicine
8600 Rockville Pike
                  Bethesda, MD 20894 Web Policies
FOIA
HHS Vulnerability Disclosure Help
Accessibility
Careers An official website of the United States government 
The .gov means it’s official.

            Federal government websites often end in .gov or .mil. Before
            sharing sensitive information, make sure you’re on a federal
            government site.
           
The site is secure.

            The https:// ensures that you are connecting to the
            official website and that any information you provide is encrypted
            and transmitted securely.
           

          Objective:
        
      
      To investigate the current epidemiology of menstrual patterns among women of fertile age.
     

          Design:
        
      
      Cross-sectional postal questionnaire study.
     

          Setting:
        
      
      County of Copenhagen, Denmark.
     

          Subjects:
        
      
      3743 women, aged 15-44, selected at random from a Danish county, who were asked to provide information on menstrual pattern during the preceding year, 1981. The response rate was 78%. Information from non-responders was obtained via telephone interviews.
     

          Results:
        
      
      In women with regular menstrual cycles, the 5th-95th centile range of usual cycle length decreased from 23-35 days in the 15-19 years age group to 23-30 days in the 40-44 years age group. Only 0.5% of regular menstruating women had a usual cycle length of less than 21 days and 0.9% had a usual cycle length of greater than 35 days. At least one cycle length of less than 21 days was experienced by 18.6%, whereas 29.5% had at least one cycle of greater than 35 days. Menstrual cycle variation of greater than 14 days was present in 29.3% of all women. Cycle length variation of greater than 14 days was 2.7 times more frequent in women from lower social groups (controlled by age).
     

          Conclusion:
        
      
      The study confirmed the normally used definitions of polymenorrhoea (cycle length less than 21 days) and oligomenorrhoea (cycle length between 36 and 90 days), as these very short or long menstrual cycle lengths were very seldom recorded for a longer period. However, the high frequency in a normal population of large menstrual cycle length variation challenges the view that an intra-individual variation of greater than 5 days should be regarded as a sign of disease in the woman.
     NCBI Literature Resources 
MeSH
PMC
Bookshelf
Disclaimer
 The PubMed wordmark and PubMed logo are registered trademarks of the U.S. Department of Health and Human Services (HHS). Unauthorized use of these marks is strictly prohibited. Connect with NLM National Library of Medicine
8600 Rockville Pike
                  Bethesda, MD 20894 Web Policies
FOIA
HHS Vulnerability Disclosure Help
Accessibility
Careers An official website of the United States government 
The .gov means it’s official.

            Federal government websites often end in .gov or .mil. Before
            sharing sensitive information, make sure you’re on a federal
            government site.
           
The site is secure.

            The https:// ensures that you are connecting to the
            official website and that any information you provide is encrypted
            and transmitted securely.
           

          Objective:
        
      
      To investigate the current epidemiology of menstrual patterns among women of fertile age.
     

          Design:
        
      
      Cross-sectional postal questionnaire study.
     

          Setting:
        
      
      County of Copenhagen, Denmark.
     

          Subjects:
        
      
      3743 women, aged 15-44, selected at random from a Danish county, who were asked to provide information on menstrual pattern during the preceding year, 1981. The response rate was 78%. Information from non-responders was obtained via telephone interviews.
     

          Results:
        
      
      In women with regular menstrual cycles, the 5th-95th centile range of usual cycle length decreased from 23-35 days in the 15-19 years age group to 23-30 days in the 40-44 years age group. Only 0.5% of regular menstruating women had a usual cycle length of less than 21 days and 0.9% had a usual cycle length of greater than 35 days. At least one cycle length of less than 21 days was experienced by 18.6%, whereas 29.5% had at least one cycle of greater than 35 days. Menstrual cycle variation of greater than 14 days was present in 29.3% of all women. Cycle length variation of greater than 14 days was 2.7 times more frequent in women from lower social groups (controlled by age).
     

          Conclusion:
        
      
      The study confirmed the normally used definitions of polymenorrhoea (cycle length less than 21 days) and oligomenorrhoea (cycle length between 36 and 90 days), as these very short or long menstrual cycle lengths were very seldom recorded for a longer period. However, the high frequency in a normal population of large menstrual cycle length variation challenges the view that an intra-individual variation of greater than 5 days should be regarded as a sign of disease in the woman.
     NCBI Literature Resources 
MeSH
PMC
Bookshelf
Disclaimer
 The PubMed wordmark and PubMed logo are registered trademarks of the U.S. Department of Health and Human Services (HHS). Unauthorized use of these marks is strictly prohibited. Connect with NLM National Library of Medicine
8600 Rockville Pike
                  Bethesda, MD 20894 Web Policies
FOIA
HHS Vulnerability Disclosure Help
Accessibility
Careers