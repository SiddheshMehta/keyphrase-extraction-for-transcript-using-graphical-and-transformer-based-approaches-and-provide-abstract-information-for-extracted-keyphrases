 An official website of the United States government 
The .gov means it’s official.

            Federal government websites often end in .gov or .mil. Before
            sharing sensitive information, make sure you’re on a federal
            government site.
           
The site is secure.

            The https:// ensures that you are connecting to the
            official website and that any information you provide is encrypted
            and transmitted securely.
           

          Purpose:
        
      
      Prior studies exploring the reliability of peak fat oxidation (PFO) and the intensity that elicits PFO (FATMAX) are often limited by small samples. This study characterised the reliability of PFO and FATMAX in a large cohort of healthy men and women.
     

          Methods:
        
      
      Ninety-nine adults [49 women; age: 35 (11) years; [Formula: see text]O2peak: 42.2 (10.3) mL·kg BM-1·min-1; mean (SD)] completed two identical exercise tests (7-28 days apart) to determine PFO (g·min-1) and FATMAX (%[Formula: see text]O2peak) by indirect calorimetry. Systematic bias and the absolute and relative reliability of PFO and FATMAX were explored in the whole sample and sub-categories of: cardiorespiratory fitness, biological sex, objectively measured physical activity levels, fat mass index (derived by dual-energy X-ray absorptiometry) and menstrual cycle status.
     

          Results:
        
      
      No systematic bias in PFO or FATMAX was found between exercise tests in the entir