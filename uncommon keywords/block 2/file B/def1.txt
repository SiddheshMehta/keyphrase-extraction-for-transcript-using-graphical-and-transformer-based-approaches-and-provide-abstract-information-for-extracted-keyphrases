 Files with the B file extension are associated with different file formats. One is used to use Grand Theft Auto 3 save game data. The other and the one we want to have a more detailed look at is a developer file that stores source code written in a specific programming language.

B files contain markup language written in BASIC (Beginner’s All-Purpose Instruction Code), a popular programming language. The files are typically saved in a plain-text format. The code inside the B file can contain the code or a program developed in BASIC as well as notes and functions that reference other files that contain BASIC scrips and programs.

B is not the only file format used for BASIC source code files. The probably more wide-spread alternative is the BAS file. 
                                        Here's a small, but not exhaustive list of programs that can open B documents:
                                     Stay connected: No software installation needed. Made in Radolfzell (Germany) by
                            QaamGo Media GmbH
 