 An official website of the United States government 
The .gov means it’s official.

            Federal government websites often end in .gov or .mil. Before
            sharing sensitive information, make sure you’re on a federal
            government site.
           
The site is secure.

            The https:// ensures that you are connecting to the
            official website and that any information you provide is encrypted
            and transmitted securely.
           Your access to PubMed Central has been blocked because you are using an automated process to retrieve content from PMC, in violation of the terms of the PMC Copyright Notice. Use of PMC is free, but must comply with the terms of the Copyright Notice on the PMC site. For additional information, or to request that your IP address be unblocked, please send an email to PMC. For requests to be unblocked, you must include all of the information in the box above in your message. Connect with NLM National Library of Medicine
8600 Rockville Pike
                  Bethesda, MD 20894 Web Policies
FOIA
HHS Vulnerability Disclosure Help
Accessibility
Careers An official website of the United States government 
The .gov means it’s official.

            Federal government websites often end in .gov or .mil. Before
            sharing sensitive information, make sure you’re on a federal
            government site.
           
The site is secure.

            The https:// ensures that you are connecting to the
            official website and that a