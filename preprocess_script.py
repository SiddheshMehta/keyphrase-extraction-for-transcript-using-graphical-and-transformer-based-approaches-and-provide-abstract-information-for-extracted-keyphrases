import utils
import json
import sys

f = open(sys.argv[1], "r")
doc = f.read()
f.close()

txt = doc.split("\n")
doc = ""
for line in txt:
	doc = doc + line + " " 

block_json = utils.get_sentence_blocks(doc[:-1])
json_formatted_str = json.dumps(block_json, indent=2)

f = open("temp.json", "w")
f.write(json_formatted_str)
f.close()
