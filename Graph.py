import networkx as nx
import nltk
from operator import itemgetter
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
import phrase_extractor as phe
import clustering as cl


class Graph:
	
	
	#initialize
	def __init__(self):
		self.G = nx.DiGraph()
		self.text = ""
		self.scores = {}
		
	
	
	#adds text forthe graph	 
	def addText(self, text):
		print(text)
		self.text = text
		return
	
	
	
	#converts the text into graphical format and builds the graph	
	def build(self):
		
		lemmatizer = WordNetLemmatizer()
		tokens = nltk.word_tokenize(self.text)
		self.text = nltk.sent_tokenize(self.text)
		n = len(self.text)
		words = set()
		
		
		for i in range(0, n):
			
			for word in words:
				self.G.add_node(word)
		
			sentence = self.text[i]
			word_bag = nltk.word_tokenize(sentence)
			for j in range(0, len(word_bag)):
				word_bag[j] = lemmatizer.lemmatize(word_bag[j])
				if(j == 0):
					word_bag[j] = word_bag[j].lower()
					continue
				try:
					self.G[word_bag[j-1]][word_bag[j]]["weight"] += 1
					
				except:
					self.G.add_edge(word_bag[j-1],word_bag[j], weight=1)					
			
					
		return
		
	
	
	#gets various statistic for each node of the graph
	def get_statistics(self):
		deg_centrality = nx.degree_centrality(self.G)
		in_deg_centrality = nx.in_degree_centrality(self.G)
		out_deg_centrality = nx.out_degree_centrality(self.G)
		close_centrality = nx.closeness_centrality(self.G)
		bet_centrality = nx.betweenness_centrality(self.G,normalized=True,endpoints=False)
		pr = nx.pagerank(self.G, alpha = 0.8)
		
		stop_words = set(stopwords.words('english'))
		words = list(self.G.nodes)
		for word in words:
			self.scores[word] = close_centrality[word]*bet_centrality[word]/(pr[word]*deg_centrality[word])
			if word in stop_words:
				self.scores[word] = 0
		
		return self.scores
		
	def rank_phrases_json(self,data_obj):
		
		lemmatizer = WordNetLemmatizer()
		stop_words = set(stopwords.words('english'))
		for key, value in data_obj.items():
			text = value['text']
			phrases = phe.extract_phrases(text)
			phrase_n_score = []
			for phrase in phrases:
				phrase_score = 0
				n = 0
				for word in phrase:
					if word not in stop_words:
						phrase_score = phrase_score + self.scores[lemmatizer.lemmatize(word)]
				phrase_n_score.append([phrase, phrase_score])
				phrase_n_score = sorted(phrase_n_score, key=itemgetter(1), reverse=True)
			data_obj[key]['graph-keyphrases'] = phrase_n_score[0:]
#			print(data_obj[key]['graph-keyphrases'])
#			print(data_obj[key]['keybert-keywords'])
#			print("\n")	
			
#		master_list = []
#		for key, value in data_obj.items():
#			master_list = value['graph-keyphrases']

			
			
			graph_keyphrase_scores = {}
			graph_keyphrases = []
			
			for ele in phrase_n_score:
				keyphrase = ""
				for word in ele[0]:
					keyphrase = keyphrase + " " + word
				graph_keyphrases.append(keyphrase[1:])
				graph_keyphrase_scores[keyphrase[1:]] = ele[1]
				 
		
			c = cl.clustering()
			c.add(graph_keyphrases)
			pred_y, optimal_cluster = c.kmeans_clusters(plot = True)
			representing_keyphrases = []
			for key1, value in optimal_cluster.items():
				cluster_items = value
				cluster_phrase_n_score = []
				for item in cluster_items:
					cluster_phrase_n_score.append([item, graph_keyphrase_scores[item]])
				max_score = 0
				repesenting_keyphrase = ""
				for ele in cluster_phrase_n_score:
					if(ele[1] > max_score):
						max_score = ele[1]
						representing_keyphrase = ele[0]
				representing_keyphrases.append([nltk.word_tokenize(representing_keyphrase), max_score])
			data_obj[key]['graph-keyphrases'] = representing_keyphrases

#f = open("../Resource/transcript1.txt", "r")
#data = f.read()

#graph = Graph()
#graph.addText(data)
#graph.build()
#print(graph.get_statistics())
