import json
import nltk
import utils
import DocSimilarity
import sys

class Pool:
	
	def __init__(self):
		self.json_obj = ""
		
	def load_file(self, filename):
		f = open(filename)
		self.json_obj = json.load(f)
		f.close()
		
		
	
	def extract_similar_phrases(self):
		
		for block_no , info in self.json_obj.items():
			kb_phrases = [j.pop(0) for j in info['dl_lr-keywords']]
			g_phrases = [j.pop(0) for j in info['graph-keyphrases']]
			
			common_phrases = []
			common_pooled_phrases = []
			uncommon_pooled_phrases = []
			for kb_phrase in kb_phrases:
				for g_phrase in g_phrases:
					is_sim = self.are_phrases_similar(set(kb_phrase), set(g_phrase))
					if(is_sim):
						common_phrase = self.get_best_phrase(kb_phrase, g_phrase)
						common_phrases.append(common_phrase)
						common_pooled_phrases.append(g_phrase)
						common_pooled_phrases.append(kb_phrase)
			temp = kb_phrases + g_phrases
			for phrase in temp:
				if phrase not in common_pooled_phrases:
					uncommon_pooled_phrases.append(phrase)
			self.json_obj[block_no]['common_keyphrases'] = common_phrases
			self.json_obj[block_no]['uncommon_keyphrases'] = uncommon_pooled_phrases
			self.json_obj[block_no]['dl_lr-keywords'] = kb_phrases
			self.json_obj[block_no]['graph-keyphrases'] = g_phrases
		json_formatted_str = json.dumps(self.json_obj, indent=2)
		f = open("temp1.json", "w")
		f.write(json_formatted_str)
		f.close()	
				
	def extract_dissimilar_phrases(self, n = 3):
		
		f = open('def_path.json')
		def_path = json.load(f)
		f.close()
		
		keyphrase_lesk_scores = {}
		for keyphrase, path in def_path.items():
			keyphrase_lesk_scores[keyphrase] = {}
			try:
				for i in range(0, 5):
					print(i, keyphrase)
					try:
						def_filename = path + "def" + str(i+1) + ".txt"
						f = open(def_filename, "r")
					except:
						continue
					data = f.read()
					f.close()
					if(len(data) < 20):
						continue
					else:
						print("YES")
						docSim = DocSimilarity.DocSim()
						sim = (docSim.get_ngram_similarity(def_filename, sys.argv[1])**(0.5))*docSim.get_vector_similarity(def_filename, sys.argv[1])
						keyphrase_lesk_scores[keyphrase][def_filename] = str(sim)						
			except:
				continue
		
		json_formatted_str = json.dumps(keyphrase_lesk_scores, indent=2)
		f = open("keyphrase_lesk_scores.json", "w")
		f.write(json_formatted_str)
		f.close()		
			
		
	
	def get_best_phrase(self, kb_phrase, g_phrase):
		if(len(kb_phrase) > len(g_phrase)):
			return kb_phrase
		elif(len(kb_phrase) < len(g_phrase)):
			return g_phrase
		else:
			return kb_phrase
	
		
	def are_phrases_similar(self, phrase1, phrase2):
		
		similar_words = set()
		sim_count = 0
		for word1 in phrase1:
			max_sim = 0
			wordX = word1
			for word2 in (phrase2 - similar_words): 
				sim = self.get_similarity(word1.lower(), word2.lower())
				if(sim > max_sim and sim >= 0.6):
					max_sim = sim
					wordX = word1
					wordY = word2
				
			if(max_sim >= 0.6):
				similar_words.add(wordX)
				similar_words.add(wordY)
				sim_count += 1
		if(sim_count >= 2):
			return True
		else:
			return False 
				
	
				
		
	def get_similarity(self, str1, str2):
		
		lcs = self.LCSubStr(str1, str2, len(str1), len(str2))
		similarity = lcs/(max(len(str1), len(str2)))
		return similarity
		
	def edit_distance(self, str1, str2, m, n):
		dp = [[0 for x in range(n + 1)] for x in range(m + 1)]
		for i in range(m + 1):
			for j in range(n + 1):
 
				if i == 0:
					dp[i][j] = j
 
				elif j == 0:
					dp[i][j] = i
 
				elif str1[i-1] == str2[j-1]:
					dp[i][j] = dp[i-1][j-1]
 
				else:
					dp[i][j] = 1 + min(dp[i][j-1], dp[i-1][j], dp[i-1][j-1])   
 
		return dp[m][n]
		
	def LCSubStr(self,X, Y,m, n):
		L = [[None]*(n+1) for i in range(m+1)]

		for i in range(m+1):
			for j in range(n+1):
				if(i == 0 or j == 0):
					L[i][j] = 0
				elif(X[i-1] == Y[j-1]):
					L[i][j] = L[i-1][j-1]+1
				else:
					L[i][j] = max(L[i-1][j] , L[i][j-1])
		return L[m][n]
		
	def extract_keyphrases(self):
		pass



#pool = Pool()
#pool.load_file("temp.json")
#pool.extract_similar_phrases()
#print("Similar phrases extracted")
#pool.extract_dissimilar_phrases()





