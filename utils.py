import nltk  
from nltk.corpus import stopwords
import GoogleScrapper
import os
import json
from time import sleep
from multiprocessing import Process
import threading
import requests
import torch
import numpy as np
from transformers import BertTokenizer, BertModel


def doc_maker(sentences):
	doc = ""
	for sentence in sentences:
		if(sentence[-1] != '.'):
			doc = doc + sentence + " . "
		else:
			doc = doc + sentence + " " 
	return doc
	
def get_sentence_blocks(doc, block_size = 5):
	sent_blocks = {}
	sentences = nltk.sent_tokenize(doc)	
	sentences = preprocess(sentences)
	for i in range(0, len(sentences),block_size):
		end = min(len(sentences), i+block_size)
		temp = doc_maker([x for x in sentences[i:end]])
		sent_blocks[i/5] = {}
		sent_blocks[i/5]["text"] = temp 
	return sent_blocks

def preprocess(sentences):
	output_sentences = []
	for sentence in sentences:
		flag = False
		for ch in sentence:
			try:
				ch = ord(ch)
			except:
				continue
			if( ( ch >= 65 and ch <= 90 ) or ( ch >= 98 and ch <= 122)):
				flag = True
			elif(ch > 255):
				flag = False
				break
			
		sentence = sentence.replace('\n', '', sentence.count("\n"))
		if(flag == True):
			output_sentences.append(sentence)
	return output_sentences	
	
	
def mod(vector):
	modulus = 0
	for dim in vector:
		modulus = modulus + dim**2
	modulus = modulus**(1/2)
	return modulus

def dot(A, B):
	dot_prod = 0
	for i in range(0, len(A)):
		dot_prod = dot_prod + A[i]*B[i]
	return dot_prod

def cosine_similarity(A, B):
	epsilon = 0.0000000000000000000001
	if(mod(A) * mod(B) <= epsilon):
		return 0
	return dot(A, B)/(mod(A) * mod(B))
	
	
def scrap_keyword_info(json_obj):
	
	path = "/home/siddhesh/Documents/BTech Project/keyword-extractor/KeyPhrase Highlighting/uncommon keywords/"
	
	def_path = {}
	
	try:
		os.rmdir(path)
		os.mkdir(path)
	except OSError as error:
		try:
			os.mkdir(path)
		except:
			pass
		pass
	
	thread_list = []
	for block_no, info in json_obj.items():
		print(block_no)
		uncommon_keyphrase = info['uncommon_keyphrases']
		block_path = path + "block " + str(block_no).split(".")[0] + "/"
		try:
			os.mkdir(block_path)
		except:
			pass
		for keyphrase in uncommon_keyphrase:
			query = ""
			for word in keyphrase:
				query = query + word + " "
			query = query[:-1]
			kp_path = block_path + query + "/"
			try:
				os.mkdir(kp_path)
			except:
				pass
			def_path[query] = kp_path
			process = Process(target = proc_daemon, args=(query, kp_path))
			process.start()
			sleep(10)
			process.terminate() 
			sleep(2)
	
	json_formatted_str = json.dumps(def_path, indent=2)
	f = open("def_path.json", "w")
	f.write(json_formatted_str)
	f.close()



def proc_daemon(query, path):
	results = GoogleScrapper.google_search(query)
	for result in results:
		thread = threading.Thread(target=GoogleScrapper.data_scrap, args=(result, path,))
		thread.start()
#	i = 0
#	for data in data_list:
#		file_path = path + "def" + str(i) + ".txt"
#		f = open(file_path, "w")
#		text = ""
#		for d in data:
#			text = text + " "+ d
#		f.write(text)
#		f.close()
#		i = i + 1

def get_phrase_embeddings(phrases):
	text = ""
	for phrase in phrases:
		text = text + phrase + "\n"
	f = open("/tmp/phrases.txt", "w")
	f.write(text[:-1])
	f.close()	 
	
	while(1):
		f = open("/tmp/phrases.txt", "r")
		data = f.read()
		if(len(data) == 0):
			sleep(2)
			break
		else:
			continue
	f = open('/tmp/phrase_embeddings.json')
	data = json.load(f)
	for key, value in data.items():
		data[key] = np.asarray(value)
	return data	
	
def pdf_to_text(pdf_filepath):
	doc = fitz.open('galvin-10ed.pdf')
	print(doc)
	text = ""
	for page in doc:
		print(page)	
		text+=page.get_text()
	print(text)
	
def get_bio_tags(doc, keyphrases):
    bio_tags = ["O" for i in range(0, len(doc))]
    count = 0
    j = 0
    for i in range(0, len(keyphrases)):
        keyphrase = keyphrases[i]
        if(len(keyphrase) == 0):
            continue
        for j in range(0, len(doc)):
            if(doc[j].lower() == keyphrase[0].lower()):
                J = j
                flag = 0
                for k in range(0, len(keyphrase)):
                    if(J < len(doc)):
                        if(keyphrase[k].lower() != doc[J].lower()):
                            flag = 1
                            break
                        else:
                            pass
                    else:
                        continue
                    J = J + 1
                if(flag == 0):
                    bio_tags[j] = 'B'
                    J = j+1
                    for x in range(1, len(keyphrase)):
                        if(J >= len(bio_tags)):
                            break
                        bio_tags[J] = "I"
                        J = J + 1
                    count = count + 1
                if(flag == 1):
                    pass
        
    return bio_tags

def get_variables(doc_list, keyphrase_list):

	tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
	model = BertModel.from_pretrained('bert-base-uncased')
	bio_tags_list = []
	n = len(doc_list)
	   
	embedding_doc = []
	j = 0
	noise = []
	for doc in doc_list:
		doc_text = doc
#		for ele in doc:
#			doc_text = doc_text + " " + ele
		tokens = nltk.word_tokenize(doc_text)
#		print(doc)
		input_ids = torch.tensor([tokenizer.convert_tokens_to_ids(tokens)])
		j = j + 1
		print("Doc no.", j)
		if(True):
			with torch.no_grad():
				outputs = model(input_ids)
				embeddings = outputs[0]
				try:
					if(len(embeddings[0]) != len(tokens) or len(embeddings[0]) != len(bio_tags_list[j-1])):
						bio_tags_list[j-1] = get_bio_tags(tokens, keyphrase_list[j-1])
					if(len(embeddings[0]) != len(tokens) or len(embeddings[0]) != len(bio_tags_list[j-1])):
						print("YES I caught u error")  
				except:
					pass  
		else:
			noise.append(j-1)
		       
	    # Extract embeddings for each token in the input text
		word_embeddings = []
		for i in range(len(tokens)):
			word_embedding = embeddings[0][i]
			word_embeddings.append(word_embedding)

		embedding_doc.append(word_embeddings)
	    
	for i in range(0, len(noise)):
		bio_tags_list.pop(noise[i]-i)
	    

	  
	time_steps = 3
	max_sequence_length = max(len(seq) for seq in embedding_doc)
	embedding_size = len(embedding_doc[0][0])

	# Initialize the data array with zeros
	num_samples = len(embedding_doc)
	X = np.zeros((num_samples, 500, embedding_size), dtype=np.float32)
	
	
# Copy the embeddings into the data array
	for i, seq in enumerate(embedding_doc):
		for j, emb in enumerate(seq):
			X[i, j, :] = np.asarray(emb, dtype=np.float32)
		   
	if(keyphrase_list[0] == None):
		return X, None
			   
	op = []
	for bio_tags in bio_tags_list:
		ele = []
		for tag in bio_tags:
			if(tag == "O"):
				ele.append([1,0,0])
			elif(tag == "B"):
				ele.append([0,1,0])
			elif(tag == "I"):
				ele.append([0,0,1])
		   
		op.append(ele)
	#op = np.array(op)



	max_sequence_length = max(len(seq) for seq in op)
	op_size = len(op[0][0])

	# Initialize the data array with zeros
	num_samples = len(op)
	Y = np.zeros((num_samples, 500, op_size), dtype=np.float32)

	# Copy the embeddings into the data array
	for i, seq in enumerate(op):
		for j, emb in enumerate(seq):
			Y[i, j, :] = np.asarray(emb, dtype=np.float32)
		   
	return X, Y

def dl_get_values(yhat):
	op = []
	for ele in yhat:
		if(ele[0] == '1'):
			op.append('0')
		else:
			 op.append('1')
			 
	return op


#f = open("temp1.json")
#json_obj = json.load(f)
#f.close()
#scrap_keyword_info(json_obj)		
#print(pdf_to_text("galvin-10ed.pdf"))
