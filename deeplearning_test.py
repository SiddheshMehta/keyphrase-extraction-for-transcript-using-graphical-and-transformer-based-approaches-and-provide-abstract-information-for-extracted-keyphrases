import tensorflow as tf

new_model = tf.keras.models.load_model('lstm_model.h5')

import utils
import json
import os
import nltk
import csv
import nltk
import torch

import pandas as pd
import numpy as np
import random
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense




    
doc_list, keyphrase_list = [], []
 
# opening the CSV file

if(True):
   
  # reading the CSV file
  
  df = pd.read_csv(r'galvin_dataset.csv', encoding = "ISO-8859-1")
  df["Blog"].str.encode('ascii', 'ignore').str.decode('ascii')
  df["Keyphrases"].str.encode('ascii', 'ignore').str.decode('ascii')
  
  for M in range(2, 6):
 
  # displaying the contents of the CSV file
  #for lines in csvFile:
        lines = [df["Blog"][M] , df["Keyphrases"][M]]
        keyphrases = [keyphr.strip('\'\'')  for keyphr in  lines[1].strip('][').split(', ')]
        text = nltk.word_tokenize(lines[0])
        doc = []
        i = 0
        while(i < len(text)):
            if(text[i][-1] != '-'):
                doc.append(text[i])
            elif(i != len(text)-1):
                doc.append(text[i][:-1]+text[i+1])
                i = i + 1
            i = i + 1
        doc_list.append(doc)
        
        k_phrases = []
        for keyphrase in keyphrases:
            keyphrase = nltk.word_tokenize(keyphrase)
            keyphr = []
            i = 0
            while(i < len(keyphrase)):
                if(keyphrase[i][-1] != '-'):
                    keyphr.append(keyphrase[i])
                elif(i != len(keyphrase)-1):
                    keyphr.append(keyphrase[i][:-1]+keyphrase[i+1])
                    i = i + 1
                i = i + 1
            k_phrases.append(keyphr)
        keyphrase_list.append(k_phrases)
            


def get_bio_tags(doc, keyphrases):
    bio_tags = ["O" for i in range(0, len(doc))]
    count = 0
    j = 0
    for i in range(0, len(keyphrases)):
        keyphrase = keyphrases[i]
        if(len(keyphrase) == 0):
            continue
        for j in range(0, len(doc)):
            if(doc[j].lower() == keyphrase[0].lower()):
                J = j
                flag = 0
                for k in range(0, len(keyphrase)):
                    if(J < len(doc)):
                        if(keyphrase[k].lower() != doc[J].lower()):
                            flag = 1
                            break
                        else:
                            pass
                    else:
                        continue
                    J = J + 1
                if(flag == 0):
                    bio_tags[j] = 'B'
                    J = j+1
                    for x in range(1, len(keyphrase)):
                        if(J >= len(bio_tags)):
                            break
                        bio_tags[J] = "I"
                        J = J + 1
                    count = count + 1
                if(flag == 1):
                    pass
        
    return bio_tags



X, Y = utils.get_variables(doc_list, keyphrase_list)

yhat = new_model.predict(X)
print(type(X), X.shape)
X = X.tolist()

bio_pred = []

bio_pred_cat = []
print(yhat)
j = -1
for ele in yhat[3]:
	j = j + 1
	if(ele[0] > ele[1] and ele[0]> ele[2]):
		bio_pred.append([1,0,0])
		bio_pred_cat.append("O")
	elif(ele[1] > ele[0] and ele[1]> ele[2]):
		bio_pred.append([0,1,0])
		bio_pred_cat.append("B")
		print(j)
	elif(ele[2] > ele[0] and ele[2]> ele[1]):
		bio_pred.append([0,0,1])
		bio_pred_cat.append("I")
		print(j)
	else:
		bio_pred.append([0,0,0])
Y = Y.tolist()

print(len(bio_pred), len(Y[0]))
count = 0
for i in range(0, len(bio_pred)):
	if(bio_pred[i] == Y[1][i]):
		count = count + 1
	
print(float(count)/len(bio_pred))

doc_text = ""
for ele in doc_list[3]:
	doc_text = doc_text + " " + ele
tokens = nltk.word_tokenize(doc_text)


for i in range(0, len(tokens)):
	cat = bio_pred_cat[i]
	if(cat == "O"):
		continue
	else:
		print(tokens[i])
	
print(len(tokens))
	
