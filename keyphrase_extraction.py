import json

def extract_keyphrases(top = 5):
	
	with open('temp1.json', 'r') as file:
		block_json = json.load(file)
		
	doc_keyphrases = {}
	
	for key, values in block_json.items():
		common_phrases = values["common_keyphrases"]
		uncommon_phrases = values["uncommon_keyphrases"]
		doc_keyphrases[key] = {}
		doc_keyphrases[key]["text"] = values["text"]
		keyphrases = []
		
		for phrase_list in common_phrases:
			phrase = ""
			for word in phrase_list:
				phrase = phrase + " " + word
			phrase = phrase[1:]
			keyphrases.append(phrase)
		
		rem = 5 - len(common_phrases)
		if(rem > 0):
			rem_phrases = extract_ranked_phrases(rem, uncommon_phrases)
			keyphrases = keyphrases + rem_phrases
		doc_keyphrases[key]["keyphrases"] = keyphrases
		
	return doc_keyphrases
		
			
def extract_ranked_phrases(top, uncommon_phrases):
	
	op = []
	
	for i in range(len(uncommon_phrases)):
		phrase = ""
		for word in uncommon_phrases[i]:
			phrase = phrase + " " + word
		uncommon_phrases[i] = phrase[1:]
		
	with open('keyphrase_lesk_scores.json', 'r') as file:
		phrase_score = json.load(file)
		for phrase in uncommon_phrases:
			score = 0
			scores = phrase_score[phrase]
			for key, value in scores.items():
				score = score + float(value)
			
			try:
				avg_score = (score*len(phrase)/len(scores))%100
			except:
				avg_score = 0 
				
			op.append([phrase, avg_score])
	
	op = [keyphr[0] for keyphr in sorted(op,key=lambda x: (x[1]))[::-1][0:top]]
	return op
	
		
		

