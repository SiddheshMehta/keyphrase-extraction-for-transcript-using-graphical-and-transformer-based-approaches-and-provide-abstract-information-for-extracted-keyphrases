import sys
from script import py_script_executor


scriptExecutor = py_script_executor()

scriptExecutor.execute_serial("preprocess_script.py", [sys.argv[1]])

scriptExecutor.execute_serial("deeplearning_logisticregr_script.py")
scriptExecutor.execute_serial("gpu_free_mem.py")

scriptExecutor.execute_parallel("phrase_embeddings.py")
scriptExecutor.execute_serial("graph_script.py" , [sys.argv[1]])
scriptExecutor.execute_serial("gpu_free_mem.py")

scriptExecutor.execute_serial("Pooling_script.py" , [sys.argv[1]])

scriptExecutor.execute_serial("keyphrase_extraction_script.py")

print("Done")
