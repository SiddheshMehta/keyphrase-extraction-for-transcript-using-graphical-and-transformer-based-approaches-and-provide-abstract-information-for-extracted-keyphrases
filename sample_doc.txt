In addition, internal programmable registers provide a high-speed cache
for main memory. The programmer (or compiler) implements the register-
allocation and register-replacement algorithms to decide which information to
keep in registers and which to keep in main memory.
Other caches are implemented totally in hardware. For instance, most
systems have an instruction cache to hold the instructions expected to be
executed next. Without this cache, the CPU would have to wait several cycles
while an instruction was fetched from main memory. For similar reasons, most
systems have one or more high-speed data caches in the memory hierarchy. We
are not concerned with these hardware-only caches in this text, since they are
outside the control of the operating system.
Because caches have limited size, cache management is an important
design problem. Careful selection of the cache size and of a replacement policy
can result in greatly increased performance, as you can see by examining Figure
1.14. Replacement algorithms for software-controlled caches are discussed in
Chapter 10.
The movement of information between levels of a storage hierarchy may be
either explicit or implicit, depending on the hardware design and the control-
ling operating-system software. For instance, data transfer from cache to CPU
and registers is usually a hardware function, with no operating-system inter-
vention. In contrast, transfer of data from disk to memory is usually controlled
by the operating system.
In a hierarchical storage structure, the same data may appear in different
levels of the storage system. For example, suppose that an integer A that is
to be incremented by 1 is located in file B, and file B resides on hard disk.
The increment operation proceeds by first issuing an I/O operation to copy the
disk block on which A resides to main memory. This operation is followed by
copying A to the cache and to an internal register. Thus, the copy of A appears
in several places: on the hard disk, in main memory, in the cache, and in an
internal register (see Figure 1.15). Once the increment takes place in the internal
register, the value of A differs in the various storage systems.
