import KeyBert as kb
import utils
import json

import torch
import os


# Call the free_gpu_memory function to free up GPU memory


f = open("../Resource/S0010938X15002085.txt")
doc = f.read()

block_json = utils.get_sentence_blocks(doc)
key_words = kb.get_keywords_block_wise(block_json)
json_formatted_str = json.dumps(block_json, indent=2)
torch.cuda.empty_cache()

f = open("temp.json", "w")
f.write(json_formatted_str)
f.close()
