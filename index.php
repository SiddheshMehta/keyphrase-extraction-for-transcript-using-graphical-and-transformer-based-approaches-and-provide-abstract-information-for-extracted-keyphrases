<!DOCTYPE html>
<html>
<head>
  <title>Keyphrase Extraction System</title>
  <style>
         body {
  font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
  background: linear-gradient(to right bottom, #c33764, #1d2671);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
}




h1 {
  text-align: center;
  margin-top: 20px;
  color: #fff;
  text-shadow: 2px 2px 5px rgba(0,0,0,0.5);
  font-size: 3rem;
}

form {
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 20px 0;
}

label {
  font-weight: bold;
  margin-bottom: 20px;
  color: #fff;
  font-size: 1.5rem;
  text-shadow: 2px 2px 5px rgba(0,0,0,0.5);
}

input[type=file] {
  border: none;
  background-color: #fff;
  padding: 20px;
  margin-bottom: 20px;
  border-radius: 10px;
  box-shadow: 2px 2px 5px rgba(0,0,0,0.5);
}

button {
  border: none;
  background-color: #004b96;
color: #f0f0f0;

  padding: 10px 20px;
  border-radius: 5px;
  cursor: pointer;
  transition: background-color 0.2s ease;
}

button:hover {
  background-color: #7b5fee;
}



hr {
  margin: 20px 0;
  border: none;
  height: 2px;
  background-color: #fff;
  opacity: 0.5;
}

#output {
  margin: 30px auto;
  padding: 20px;
  border: 2px solid #fff;
  border-radius: 10px;
  background-color: rgba(255, 255, 255, 0.8);
  max-width: 800px;
  box-shadow: 2px 2px 5px rgba(0,0,0,0.5);
}

.highlighted {
  background-color: #ffff00;
  cursor: pointer;
  padding: 2px 5px;
  border-radius: 3px;
  transition: background-color 0.2s ease;
  box-shadow: 1px 1px 3px rgba(0,0,0,0.5);
  font-weight: bold;
}

.highlighted:hover {
  background-color: #ffea00;
}

@media screen and (max-width: 768px) {
  h1 {
    font-size: 2rem;
  }
  
  label {
    font-size: 1.2rem;
  }
  
  input[type=file] {
    padding: 10px;
  }
  
  button {
    padding: 10px 20px;
    font-size: 1rem;
  }
  
  #output {
    margin: 30px auto;
    padding: 10px;
  }
  
  .highlighted {
    padding: 1px 3px;
  }
}

  </style>
</head>
<body>
  <h1>Keyphrase Extraction System</h1>
  <form>
    <label for="file">Select a JSON file:</label>
    <input type="file" id="file" name="file" accept=".json">
    <button type="button" id="load-btn">Load JSON</button>
  </form>
  <hr>
  <div id="output"></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.8.335/pdf.min.js"></script>
    <script>
      const loadBtn = document.querySelector('#load-btn');
const outputDiv = document.querySelector('#output');

loadBtn.addEventListener('click', () => {
  const file = document.querySelector('#file').files[0];
  if (!file) {
    alert('Please select a file!');
    return;
  }
  
  const reader = new FileReader();
  reader.addEventListener('load', (event) => {
    const jsonData = JSON.parse(event.target.result);
    outputDiv.innerHTML = generateOutputHTML(jsonData);
    addClickListenersToHighlightedKeys();
  });
  reader.readAsText(file);
});

function generateOutputHTML(jsonData) {
  let outputHTML = '';
  for (const [key, value] of Object.entries(jsonData)) {
    const text = value.text;
    const keyphrases = value.keyphrases;
    outputHTML += `<div><strong>${key}:</strong></div>`;
    outputHTML += `<div>${highlightKeys(text, keyphrases)}</div>`;
  }
  return outputHTML;
}
function highlightKeys(text, keyphrases) {
  let highlightedText = text;
  for (const [key, value] of Object.entries(keyphrases)) {
    const regex = new RegExp(`\\b${key}\\b`, 'i');
    const match = highlightedText.match(regex);
    if (match) {
      const startPos = match.index;
      const endPos = startPos + match[0].length;
      highlightedText = `${highlightedText.slice(0, startPos)}<span class="highlighted" data-value="${value}">${match[0]}</span>${highlightedText.slice(endPos)}`;
    }
  }
  return highlightedText;
}

function addClickListenersToHighlightedKeys() {
  const highlightedSpans = document.querySelectorAll('.highlighted');
  for (const span of highlightedSpans) {
    span.addEventListener('click', () => {
      const value = span.getAttribute('data-value');
      alert(value);
    });
  }
}


        </script>
          </body>
        </html> 
        