from pdfminer.high_level import extract_pages
from pdfminer.layout import LTTextContainer, LTChar,LTLine,LAParams
import os
import nltk
from keyphrasetransformer import KeyPhraseTransformer
import csv
import string


def remove_non_ascii(a_str):
    ascii_chars = set(string.printable)
    return ''.join(filter(lambda x: x in ascii_chars, a_str))
    
kp = KeyPhraseTransformer()

path= 'dataset/pdfs/James W. Kurose, Keith W. Ross - Computer Networking_ A Top-Down Approach-Pearson -8th-edition(2021).pdf'
op = []
indexList = [0]
i = 1
for page_layout in extract_pages(path):
	for element in page_layout:
		if isinstance(element, LTTextContainer):
			try:
				charsize = 0
				for text_line in element:
					for character in text_line:
						if isinstance(character, LTChar):
							#print(character.fontname)
							#print(character.size)
							charsize = character.size
							
#					print(text_line.get_text()[:-1], round(charsize, 1))		
					op.append([remove_non_ascii(text_line.get_text()[:-1]), round(charsize, 1)]) 
					if(len(op) > 1):
						if(op[-2][1] != op[-1][1]):
							indexList.append(i)
					i = i + 1
			except:
#				print("Line fail")		
				pass                   
				

chunks = []	
for i in range(1, len(indexList)):
	temp = []
	if(indexList[i] - indexList[i-1] > 5):
		for j in range(indexList[i-1], indexList[i]-1):
			temp.append(op[j][0])
	chunks.append(temp)
	
doc = []
for ele in chunks:
	text = ""
	for phrase in ele:
		text = text + phrase + " "
	sentences = nltk.sent_tokenize(text)
	block = ""
	if(len(sentences) > 15):
		for i in range(0, len(sentences)):
			if((i+1)%10 == 0):
				block = block + " " + sentences[i]
				doc.append(block)
				block = ""
			else:
				block = block + " " + sentences[i]
	elif(len(sentences) >= 7):
		doc.append(text)

print(len(doc))

fields = ['Blog', 'Keyphrases'] 
rows = []

i = 1
for text in doc:
	keyphrase = kp.get_key_phrases(text)
	print(i,keyphrase)
	i = i + 1
	rows.append([text, keyphrase])

#f = open("dataset/pdfs/galvin-10ed.txt", "w")
#f.write(doc)
#f.close()	


# name of csv file 
filename = "kurusros.csv"
    
# writing to csv file 
with open(filename, 'w') as csvfile: 
    # creating a csv writer object 
	csvwriter = csv.writer(csvfile) 
        
    # writing the fields 
	csvwriter.writerow(fields) 
        
    # writing the data rows 
	csvwriter.writerows(rows)	
