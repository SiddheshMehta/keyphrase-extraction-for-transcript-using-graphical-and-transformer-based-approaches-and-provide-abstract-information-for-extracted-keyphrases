import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split

# Load and preprocess data
data = pd.read_csv(r'galvin_dataset.csv', encoding = "ISO-8859-1")
data["Blog"].str.encode('ascii', 'ignore').str.decode('ascii')
data["Keyphrases"].str.encode('ascii', 'ignore').str.decode('ascii')

for M in range(0, len(data["Blog"])):
 
  # displaying the contents of the CSV file
  #for lines in csvFile:
	lines = [data["Blog"][M] , data["Keyphrases"][M]]
	keyphrases = [keyphr.strip('\'\'')  for keyphr in  lines[1].strip('][').split(', ')]
	data["Keyphrases"][M] = keyphrases

X = data['Blog'].values
y = data['Keyphrases'].values


# Tokenize input text
tokenizer = Tokenizer()
tokenizer.fit_on_texts(X)
X = tokenizer.texts_to_sequences(X)
X = pad_sequences(X, padding='post', maxlen=100)
for ls in y:
	for ele in ls:
		print(ele)


# Split data into train and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
# Define deep learning model
model = tf.keras.Sequential([
    tf.keras.layers.Embedding(input_dim=len(tokenizer.word_index)+1, output_dim=100, input_length=100),
    tf.keras.layers.Conv1D(128, 5, activation='relu'),
    tf.keras.layers.GlobalMaxPooling1D(),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dropout(0.5),
    tf.keras.layers.Dense(1, activation='sigmoid')
])

# Compile model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# Train model
model.fit(tf.convert_to_tensor(X_train), tf.convert_to_tensor(y_train), batch_size=32, epochs=10, validation_split=0.2)

# Evaluate model
loss, accuracy = model.evaluate(X_test, y_test)

# Predict keyphrases for new text
new_text = 'This is a new text to extract keyphrases from.'
new_text = tokenizer.texts_to_sequences([new_text])
new_text = pad_sequences(new_text, padding='post', maxlen=100)
keyphrases = model.predict(new_text)
