#!/usr/bin/env python
# coding: utf-8

# In[1]:


from sentence_transformers import SentenceTransformer, util
import numpy as np
import nltk
import torch

torch.cuda.empty_cache()
model = SentenceTransformer('stsb-roberta-large')
embedding1 = model.encode("The early Dravidian religion constituted a non-Vedic form of Hinduism in that they were either historically or are at present Āgamic.", convert_to_tensor=True)
print(embedding1)
print(len(embedding1))


# In[2]:


sentence1 = "The quick brown fox jumps over the lazy dog"
sentence2 = "The boss fired the unloyal staff"

def get_sentence_similarity(model, sent1, sent2):
  embedding1 = model.encode(sentence1, convert_to_tensor=True)
  embedding2 = model.encode(sentence2, convert_to_tensor=True)
  cosine_scores = util.pytorch_cos_sim(embedding1, embedding2)
  return cosine_scores.item()

print(get_sentence_similarity(model, sentence1, sentence2))


# In[ ]:


import json    
while(True):
    try:
        f = open("/tmp/phrases.txt", "r")
    except:
        continue
    data = f.read()
    if(len(data) < 4):
        continue
    f.close()
    
    phrases = data.split("\n")
    print(phrases)
    phrase_embeddings = {}
    for phrase in phrases:
        phrase_embeddings[phrase] = model.encode(phrase).tolist()
        
    f = open("/tmp/phrases.txt", "w")
    f.write("")
    f.close()    
    
    json_formatted_str = json.dumps(phrase_embeddings, indent=2)
    f = open("/tmp/phrase_embeddings.json", "w")
    f.write(json_formatted_str)
    f.close()


# In[ ]:





# In[ ]:




