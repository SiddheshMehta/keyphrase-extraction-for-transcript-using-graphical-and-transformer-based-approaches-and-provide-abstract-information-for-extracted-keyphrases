import nltk
from nltk.corpus import stopwords
from stop_words import get_stop_words
from nltk.corpus import wordnet as wn

#text = "India is fifth largest economy in the world"

#bigrm = list(nltk.bigrams(text.split()))

class ngrams:
	
	def __init__(self):
		self.stop_words = list(get_stop_words('en'))         
		nltk_words = list(stopwords.words('english'))  + ['.', ',' , '?', '!'] 
		self.stop_words.extend(nltk_words)
		self.exhaustive_bigrams = []
		self.exhaustive_trigrams = []
		
	def get_unigrams(self, doc):
		return list(set(nltk.word_tokenize(doc)) - set(self.stop_words))
		
	def get_bigrams(self, doc):
		return list(nltk.bigrams(doc.split()))
		
	def get_trigrams(self, doc):
		return list(nltk.trigrams(doc.split()))
		
	def get_exhaustive_unigrams(self, doc):
		unigrams = self.get_unigrams(doc)
		exhaustive_unigrams = set()
		for unigram in unigrams:
			lst = set([word.name().split(".")[0] for word in wn.synsets(unigram)])
			names = set([word.name() for word in wn.synsets(unigram)])
			exhaustive_unigrams = exhaustive_unigrams.union(lst)
			definitions = [wn.synset(word).definition() for word in names]
			examples = [wn.synset(word).examples() for word in names]
			
			for definition in definitions:
#				print(definition)
				exhaustive_unigrams = exhaustive_unigrams.union(set(self.get_unigrams(definition)))
				
			for example in examples:
				for ex in example:
					exhaustive_unigrams = exhaustive_unigrams.union(set(self.get_unigrams(ex)))			
				
		return exhaustive_unigrams
			
		
		
	def get_exhaustive_bigrams(self, doc):
		bigrams = self.get_bigrams(doc)
		self.exhaustive_bigrams = []
		lst = []
		for bigram in bigrams:
			self.combinations(bigram, 0, lst)
			self.exhaustive_bigrams.append(bigram)
			
		return self.exhaustive_bigrams
		
	
	def get_exhaustive_trigrams(self, doc):	
		trigrams = self.get_trigrams(doc)
		self.exhaustive_trigrams = []
		lst = []
		for trigram in trigrams:
			self.combinations(trigram, 0, lst)
			self.exhaustive_trigrams.append(trigram)
					
		return self.exhaustive_trigrams
		
		
	def combinations(self, ngram_list, n, exh_ngram_list):
		 if(n >= len(ngram_list)):
		 	if(len(ngram_list) == 2):
		 		self.exhaustive_bigrams.append(tuple(exh_ngram_list))
		 	elif(len(ngram_list) == 3):
		 		self.exhaustive_trigrams.append(tuple(exh_ngram_list))
		 	return 
		 	
		 tword = ngram_list[n]
		 words = set([word.name().split(".")[0] for word in wn.synsets(tword)])
		 
		 for word in words:
		 	temp = [word for word in exh_ngram_list]
		 	temp.append(word)
		 	self.combinations(ngram_list, n+1, temp)
		 
		
#ngrm = ngrams()
#ngrm.get_exhaustive_trigrams(text)

