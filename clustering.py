from sklearn.cluster import MiniBatchKMeans
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score
from matplotlib import pyplot as plt
import utils
from DocSimilarity import DocSim



def mbkmeans_clusters(X, k, mb, print_silhouette_values):
    
    km = MiniBatchKMeans(n_clusters=k, batch_size=mb).fit(X)
    print(f"For n_clusters = {k}")
    print(f"Silhouette coefficient: {silhouette_score(X, km.labels_):0.2f}")
    print(f"Inertia:{km.inertia_}")

    if print_silhouette_values:
        sample_silhouette_values = silhouette_samples(X, km.labels_)
        print(f"Silhouette values:")
        silhouette_values = []
        for i in range(k):
            cluster_silhouette_values = sample_silhouette_values[km.labels_ == i]
            silhouette_values.append(
                (
                    i,
                    cluster_silhouette_values.shape[0],
                    cluster_silhouette_values.mean(),
                    cluster_silhouette_values.min(),
                    cluster_silhouette_values.max(),
                )
            )
        silhouette_values = sorted(
            silhouette_values, key=lambda tup: tup[2], reverse=True
        )
        for s in silhouette_values:
            print(
                f"    Cluster {s[0]}: Size:{s[1]} | Avg:{s[2]:.2f} | Min:{s[3]:.2f} | Max: {s[4]:.2f}"
            )
    return km, km.labels_




#clustering, cluster_labels = mbkmeans_clusters(X, k=2, mb=4, print_silhouette_values=True)
#print(clustering.predict(X))






class clustering:
	
	def __init__(self):
		self.phrases = None
		self.phrase_embeddings = None
		
	def add(self, phrases):
		self.phrases = phrases
#		self.phrases = ["calculated oxide thickness", "oxide thickness calculated", "surface profile modify", "roughness profile lengths", "house on the top of haunted hill", "ghost in the graveyard", "surface profile length", "oxidation coefficient calculation", "horror nights", "volume profile", "computer science and engineering", "Death zone"]
#		self.phrases = ["calculated oxide thickness", "computer science and engineering", "horror nights", "volume profile"]
		self.phrase_embeddings = utils.get_phrase_embeddings(self.phrases)
		self.X = []
		for phrase in self.phrases:
			self.X.append(self.phrase_embeddings[phrase])

	def kmeans_clusters(self, plot):
		wcss = []
		max_cluster_score = -11
		optimal_pred_y = []
		optimal_cluster = {}
		try:
			if(True):
				
				for i in range(1, min(len(self.phrases),7)):
				    kmeans = KMeans(n_clusters=i, init='k-means++', max_iter=30, n_init=30, random_state=0)
				    kmeans.fit(self.X)
				    pred_y = kmeans.fit_predict(self.X)
				    clusters = self.get_clusters(pred_y)
				    score = self.get_lesk_cluster_score(clusters)
				    cluster_score = kmeans.inertia_ * ((max(pred_y)+1)**2)* score / (1000)
				    wcss.append(cluster_score)
				    if(max_cluster_score < cluster_score):
				        max_cluster_score = cluster_score
				        optimal_pred_y = pred_y 
				        optimal_cluster = clusters
			return optimal_pred_y, optimal_cluster   		
		except:
			print("YES")
			return 

		if(plot):
			plt.plot(range(1, len(self.phrases)), wcss)
			plt.title('Elbow Method')
			plt.xlabel('Number of clusters')
			plt.ylabel('WCSS')
			plt.show()

	def get_clusters(self, y_pred):
		n = max(y_pred)
		clusters = {}
		for i in range(0, n+1):
			clusters[i] = []
		
		i = 0
		for cluster_no in y_pred:
			clusters[cluster_no].append(self.phrases[i])
			i = i + 1
			
		return clusters

	def get_lesk_cluster_score(self, clusters):
		docSim = DocSim()
		n = len(clusters)
		similarity = 0
		for i in range(0, n):
			cluster = clusters[i]
			for j in range(0, len(cluster)):
				for k in range(j + 1, len(cluster)):
					self.write_temp_file(cluster[j], "/tmp/phrase1.txt")
					self.write_temp_file(cluster[k], "/tmp/phrase2.txt")
					similarity = similarity + docSim.get_ngram_similarity("/tmp/phrase1.txt", "/tmp/phrase2.txt")
		
		dissim = 0
		for i in range(0, n):
			cluster1 = clusters[i]
			for j in range(i+1, n):
				cluster2 = clusters[j]
				for x1 in range(0, len(cluster1)):
					for x2 in range(0, len(cluster2)):
						self.write_temp_file(cluster1[x1], "/tmp/phrase1.txt")
						self.write_temp_file(cluster2[x2], "/tmp/phrase2.txt")
						dissim = dissim + docSim.get_ngram_similarity("/tmp/phrase1.txt", "/tmp/phrase2.txt")
		
			
		score = similarity - dissim/n
		return score
		
	def write_temp_file(self,phrase, filename):
		f1 = open(filename, "w")
		f1.write(phrase)
		f1.close()

#c = clustering()
#c.add([])
#c.kmeans_clusters(plot = True)

