import KeyBert as kb
import utils
import json
import Graph


f = open("../Resource/S0010938X15002085.txt")
doc = f.read()

block_json = utils.get_sentence_blocks(doc)
key_words = kb.get_keywords_block_wise(block_json)

graph = Graph.Graph()
graph.addText(doc)
graph.build()
scores = graph.get_statistics()
graph.rank_phrases_json(block_json)

json_formatted_str = json.dumps(block_json, indent=2)
f = open("temp.json", "w")
f.write(json_formatted_str)
f.close()
