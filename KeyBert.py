from keybert import KeyBERT
import nltk

def get_keywords_block_wise(block_json, no_of_kw = 5):
	
	kw_model = KeyBERT()
	
	for key, items in block_json.items():
		text = items["text"]
		keywords = kw_model.extract_keywords(text, keyphrase_ngram_range=(1,3), use_mmr = True,top_n=5)
		for i in range(0, len(keywords)):
			keywords[i] = list(keywords[i])
			keywords[i][0] = nltk.word_tokenize(keywords[i][0])
			print(keywords[i][0])
		block_json[key]["keybert-keywords"] = keywords
		
	return block_json	 
