import nltk
import utils
import fasttext
import ngrams
from nltk.corpus import stopwords
from stop_words import get_stop_words


class DocSim:

	def __init__(self):
		return
		
	def word2vec(self,file_name):
		vector = fasttext.train_supervised(file_name)
		return vector
		
	def get_max_vector_similarity(self, doc1_file, doc2_file):
		
		doc1_vector = self.word2vec(doc1_file)
		doc2_vector = self.word2vec(doc2_file)
		
		f = open(doc1_file, "r")
		doc1 = f.read()
		f.close()
		
		f = open(doc2_file, "r")
		doc2 = f.read()
		f.close()
		
		stop_words = list(get_stop_words('en'))         
		nltk_words = list(stopwords.words('english'))  + ['.', ',' , '?', '!'] 
		stop_words.extend(nltk_words)
		
		doc1_tokens = list(set(nltk.word_tokenize(doc1)) - set(stop_words))
		doc2_tokens = list(set(nltk.word_tokenize(doc2)) - set(stop_words))
		
		n = len(doc1_tokens)
		m = len(doc2_tokens)
		
		#creating similarity matrix
		sim_matrix = self.get_matrix(n, m)
		for i in range(0, n):
			for j in range(0, m):
				sim_matrix[i][j] = min(utils.cosine_similarity(doc1_vector[doc1_tokens[i]], doc2_vector[doc2_tokens[j]]),1)
				
		#initializing DP matrix
		DP = self.get_matrix(n, m)
		for i in range(0, n):
			for j in range(0,m):
				if(j == 0):
					DP[i][j] =  sim_matrix[i][j]
	#				print(DP[i][j] , doc1_tokens[i] ,doc2_tokens[j])
				else:
					DP[i][j] = 0
				
		max_sim = 0
		#filling DP matrix
		for j in range(1, m):
			for i in range(0, n):
				for k in range(0, n):
					DP[i][j] = max(DP[i][j], DP[k][j-1] + sim_matrix[i][j])
					max_sim = max(DP[i][j], max_sim)
					
		return max_sim

			
	def get_matrix(self, n, m):
		
		matrix = []
		for i in range(0, n):
			temp = []
			for  j in range(0, m):
				temp.append(0)
			matrix.append(temp)
			
		return matrix
		
	def print_matrix(self,matrix, n, m):
		for i in range(0, n):
			for j in range(0, m):
				print("{:.2f}".format(matrix[i][j]), " ", end = "")
			print()
		
	def get_vector_similarity(self, doc1_file, doc2_file):		
			
		doc1_vector = self.word2vec(doc1_file)
		doc2_vector = self.word2vec(doc2_file)
		
		f = open(doc1_file, "r")
		doc1 = f.read()
		f.close()
		
		f = open(doc2_file, "r")
		doc2 = f.read()
		f.close()
		
		stop_words = list(get_stop_words('en'))         
		nltk_words = list(stopwords.words('english'))  + ['.', ',' , '?', '!'] 
		stop_words.extend(nltk_words)
		
		doc1_tokens = list(set(nltk.word_tokenize(doc1)) - set(stop_words))
		doc2_tokens = list(set(nltk.word_tokenize(doc2)) - set(stop_words))
		
		count = 0
		sim = 0
		for token1 in doc1_tokens:
			for token2 in doc2_tokens:
				sim = sim + utils.cosine_similarity(doc1_vector[token1], doc2_vector[token2])
				count = count + 1
		avgsim = 0
		try:
			avgsim = sim/count
		except:
			avgsim = 0
			
		return sim
		
	def get_similar_tuple_cnt(self, tuple_list1, tuple_list2):
		
		score = 0
		for tuple1 in tuple_list1:
			for tuple2 in tuple_list2:
				if(tuple1 == tuple2):
					score = score + 1
		return score
	
	def get_ngram_similarity(self, prob_doc_filename, target_doc_filename):
	
		f = open(prob_doc_filename, "r")
		prob_doc = f.read()
		f.close()
		
		f = open(target_doc_filename, "r")
		target_doc = f.read()
		f.close()
		
		ngrm = ngrams.ngrams()
		prob_doc_1gram = ngrm.get_exhaustive_unigrams(prob_doc)
		prob_doc_2gram = ngrm.get_exhaustive_bigrams(prob_doc)
		prob_doc_3gram = ngrm.get_exhaustive_trigrams(prob_doc)
		
		target_doc_1gram = ngrm.get_exhaustive_unigrams(target_doc)
		target_doc_2gram = ngrm.get_exhaustive_bigrams(target_doc)
		target_doc_3gram = ngrm.get_exhaustive_trigrams(target_doc)
		
		ngram_sim_score = 0
		ngram_sim_score = self.get_similar_tuple_cnt(prob_doc_1gram , target_doc_1gram)* 1**2
		ngram_sim_score = ngram_sim_score + self.get_similar_tuple_cnt(prob_doc_2gram , target_doc_2gram)* 2**2 
		ngram_sim_score = ngram_sim_score + self.get_similar_tuple_cnt(prob_doc_3gram , target_doc_3gram)* 3**2
		
		return ngram_sim_score	
		 
		
#docSim = DocSim()
#print(docSim.get_vector_similarity("test_text1.txt", "test_text2.txt"))
#print(docSim.get_ngram_similarity("test_text1.txt", "test_text2.txt"))
